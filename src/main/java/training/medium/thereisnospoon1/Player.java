package training.medium.thereisnospoon1;

import java.util.Scanner;

public class Player {

  public static void main(String args[]) {
    new Player().run();
  }

  void run() {
    Scanner in = new Scanner(System.in);
    int width = in.nextInt(); // the number of cells on the X axis
    in.nextLine();
    int height = in.nextInt(); // the number of cells on the Y axis
    in.nextLine();
    String[][] grid = new String[height][width];
    for (int i = 0; i < height; i++) {
      String line = in.nextLine(); // width characters, each either 0 or .
      for (int j = 0; j < line.length(); j++) {
        grid[i][j] = String.valueOf(line.charAt(j));
      }
    }

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < grid.length; i++) {
      for (int j = 0; j < grid[i].length; j++) {
        if ("0".equalsIgnoreCase(grid[i][j])) {
          sb.append(j + " " + i + " ");
          sb.append(findRightNeighboor(i, j, grid));
          sb.append(findBottomNeighboor(i, j, grid));
          sb.append("\n");
        }
      }
    }
    System.out.println(sb.toString());
  }

  String findRightNeighboor(int i, int j, String[][] grid) {
    for (int k = j + 1; k < grid[i].length; k++) {
      if ("0".equalsIgnoreCase(grid[i][k])) {
        return k + " " + i + " ";
      }
    }
    return "-1 -1 ";
  }

  String findBottomNeighboor(int i, int j, String[][] grid) {
    for (int k = i + 1; k < grid.length; k++) {
      if ("0".equalsIgnoreCase(grid[k][j])) {
        return j + " " + k;
      }
    }
    return "-1 -1";
  }

}
