package training.medium.skynetrevolution1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Player {

  public static void main(String args[]) {
    new Player().run();
  }

  void run() {
    Scanner in = new Scanner(System.in);
    int N = in.nextInt(); // the total number of nodes in the level, including the gateways
    int L = in.nextInt(); // the number of links
    int E = in.nextInt(); // the number of exit gateways
    List<Link> links = new ArrayList<Link>(L);
    for (int i = 0; i < L; i++) {
      int N1 = in.nextInt(); // N1 and N2 defines a link between these nodes
      int N2 = in.nextInt();
      Link link = new Link(N1, N2);
      links.add(link);
    }
    int[] exits = new int[E];
    for (int i = 0; i < E; i++) {
      int EI = in.nextInt(); // the index of a gateway node
      exits[i] = EI;
    }

    // game loop
    while (true) {
      int SI = in.nextInt(); // The index of the node on which the Skynet agent is positioned this turn
      cutClosestLink(SI, exits, links);
    }
  }

  void cutClosestLink(int SI, int[] exits, List<Link> links) {
    boolean cut = false;
    for (int i = 0; i < exits.length && !cut; i++) {
      Link test = new Link(SI, exits[i]);
      int index = links.indexOf(test);
      if (index >= 0) {
        links.get(index).cut();
        links.remove(index);
        cut = true;
      }
    }
    if (!cut) {
      links.get(0).cut();
      links.remove(0);
    }
  }

  class Link {

    private int m_n1;

    private int m_n2;

    public Link(int n1, int n2) {
      m_n1 = n1;
      m_n2 = n2;
    }

    void cut() {
      System.out.println(m_n1 + " " + m_n2);
    }

    @Override
    public boolean equals(Object o) {
      if (!(o instanceof Link)) {
        return false;
      }
      Link oLink = (Link)o;
      if (this == oLink) {
        return true;
      }
      return (m_n1 == oLink.m_n1 && m_n2 == oLink.m_n2) || (m_n1 == oLink.m_n2 && m_n2 == oLink.m_n1);
    }

  }

}
