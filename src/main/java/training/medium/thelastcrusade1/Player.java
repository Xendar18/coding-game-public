package training.medium.thelastcrusade1;

import java.util.Scanner;

class Player {

  Scanner in = new Scanner(System.in);

  Room[][] tunnel;

  public static void main(String args[]) {
    Player player = new Player();
    player.init();
    player.run();
  }

  void init() {
    // Number of columns.
    int columnsNbr = in.nextInt();
    // Number of rows.
    int linesNbr = in.nextInt();
    if (in.hasNextLine()) {
      in.nextLine();
    }
    tunnel = new Room[columnsNbr][linesNbr];
    for (int i = 0; i < linesNbr; i++) {
      // Represents a line in the grid and contains W integers. Each integer represents one room of a given type.
      String line = in.nextLine();
      String[] lineTokens = line.split(" ");
      for (int j = 0; j < columnsNbr; j++) {
        tunnel[j][i] = new Room(RoomType.fromValue(Integer.parseInt(lineTokens[j])), j, i);
      }
    }
    // The coordinate along the X axis of the exit (not useful for this first mission, but must be read).
    int EX = in.nextInt();
  }

  void run() {
    // Game loop
    while (true) {
      int indyX = in.nextInt();
      int indyY = in.nextInt();
      String indyPos = in.next();

      // One line containing the X Y coordinates of the room in which you believe Indy will be on the next turn.
      String direction = findNextRoom(indyX, indyY, indyPos);
      System.out.println(direction);
    }
  }

  String findNextRoom(int indyX, int indyY, String indyPos) {
    int nextRoomX = indyX;
    int nextRoomY = indyY;
    Room room = tunnel[indyX][indyY];
    if (indyPos.equals(Entry.TOP.name())) {
      switch (room.getRoomType()) {
      case TYPE_1:
      case TYPE_3:
      case TYPE_7:
      case TYPE_9:
        nextRoomY++;
        break;
      case TYPE_4:
      case TYPE_6:
      case TYPE_10:
        // TYPE_6 could go right as well
        nextRoomX--;
        break;
      case TYPE_5:
      case TYPE_11:
        nextRoomX++;
        break;
      default:
        break;
      }
    } else if (indyPos.equals(Entry.LEFT.name())) {
      switch (room.getRoomType()) {
      case TYPE_1:
      case TYPE_5:
      case TYPE_8:
      case TYPE_9:
      case TYPE_13:
        nextRoomY++;
        break;
      case TYPE_2:
      case TYPE_6:
        nextRoomX++;
        break;
      default:
        break;
      }
    } else if (indyPos.equals(Entry.RIGHT.name())) {
      switch (room.getRoomType()) {
      case TYPE_1:
      case TYPE_4:
      case TYPE_7:
      case TYPE_8:
      case TYPE_12:
        nextRoomY++;
        break;
      case TYPE_2:
      case TYPE_6:
        nextRoomX--;
        break;
      default:
        break;
      }
    }
    return nextRoomX + " " + nextRoomY;
  }

  class Room {

    private RoomType type;

    private int row;

    private int column;

    Room(RoomType type, int column, int row) {
      this.type = type;
      this.column = column;
      this.row = row;
    }

    RoomType getRoomType() {
      return type;
    }

    int getColumn() {
      return column;
    }

    int getRow() {
      return row;
    }

  }

  enum Entry {
    TOP,
    LEFT,
    RIGHT;
  }

  enum RoomType {
    TYPE_0(0),
    TYPE_1(1),
    TYPE_2(2),
    TYPE_3(3),
    TYPE_4(4),
    TYPE_5(5),
    TYPE_6(6),
    TYPE_7(7),
    TYPE_8(8),
    TYPE_9(9),
    TYPE_10(10),
    TYPE_11(11),
    TYPE_12(12),
    TYPE_13(13);

    private final int type;

    private RoomType(int type) {
      this.type = type;
    }

    static RoomType fromValue(int value) {
      for (RoomType roomType : RoomType.values()) {
        if (roomType.type == value) {
          return roomType;
        }
      }
      return null;
    }

  }

}
