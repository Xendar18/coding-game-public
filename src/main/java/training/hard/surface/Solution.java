package training.hard.surface;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class Solution {

  Scanner in = new Scanner(System.in);

  Grid grid;

  public static void main(String args[]) {
    Solution solution = new Solution();
    solution.init();
    solution.run();
  }

  void init() {
    grid = new Grid(in);
  }

  void run() {
    int N = in.nextInt();
    for (int i = 0; i < N; i++) {
      int X = in.nextInt();
      int Y = in.nextInt();
      floodFill(X, Y);
      System.out.println(grid.getCell(X, Y).getSurface());
    }
  }

  void floodFill(int coordX, int coordY) {
    Cell rootCell = grid.getCell(coordX, coordY);
    if (rootCell != null && rootCell.isWater()) {
      if (rootCell.getSurface() == 0) {
        Set<Cell> lakeCells = new HashSet<>();
        Queue<Cell> cellsToAnalyze = new ArrayDeque<>();
        int lakeSurface = 0;
        cellsToAnalyze.add(rootCell);
        while (!cellsToAnalyze.isEmpty()) {
          Cell cell = cellsToAnalyze.poll();
          if (cell.isWater() && !lakeCells.contains(cell)) {
            lakeSurface++;
            lakeCells.add(cell);
            for (Direction direction : Direction.values()) {
              Cell newCell = grid.getCell(cell, direction);
              if (newCell != null) {
                cellsToAnalyze.add(newCell);
              }
            }
          }
        }
        for (Cell lakeCell : lakeCells) {
          lakeCell.setSurface(lakeSurface);
        }
      }
    }
  }

  class Grid {

    final int width;

    final int height;

    Cell[][] land;

    Grid(Scanner in) {
      int L = in.nextInt();
      int H = in.nextInt();
      width = L;
      height = H;
      if (in.hasNextLine()) {
        in.nextLine();
      }
      land = new Cell[L][H];
      for (int i = 0; i < H; i++) {
        String row = in.nextLine();
        for (int j = 0; j < L; j++) {
          land[j][i] = new Cell(j, i, row.charAt(j));
        }
      }
    }

    Cell getCell(Coordinate coordinate) {
      return getCell(coordinate.coordX, coordinate.coordY);
    }

    Cell getCell(int coordX, int coordY) {
      Cell cell = null;
      if (cellExists(coordX, coordY)) {
        cell = land[coordX][coordY];
      }
      return cell;
    }

    Cell getCell(Cell cell, Direction direction) {
      Cell newCell = null;
      int newCoordX = cell.coordinate.coordX;
      int newCoordY = cell.coordinate.coordY;
      switch (direction) {
      case NORTH:
        newCoordY--;
        break;
      case EAST:
        newCoordX++;
        break;
      case SOUTH:
        newCoordY++;
        break;
      case WEST:
        newCoordX--;
        break;
      }
      if (cellExists(newCoordX, newCoordY)) {
        newCell = land[newCoordX][newCoordY];
      }
      return newCell;
    }

    boolean cellExists(int coordX, int coordY) {
      return coordX >= 0 && coordX < width && coordY >= 0 && coordY < height;
    }

  }

  class Cell {

    private Coordinate coordinate;

    private CellType type;

    private Lake lake;

    Cell(int coordX, int coordY, char typeId) {
      coordinate = new Coordinate(coordX, coordY);
      if ('#' == typeId) {
        this.type = CellType.EARTH;
      } else if ('O' == typeId) {
        this.type = CellType.WATER;
      }
      lake = new Lake();
    }

    boolean isWater() {
      return CellType.WATER == type;
    }

    int getSurface() {
      return lake.surface;
    }

    void setSurface(int surface) {
      lake.surface = surface;
    }

    @Override
    public boolean equals(Object obj) {
      Cell other = (Cell)obj;
      return coordinate.equals(other.coordinate);
    }

  }

  class Coordinate {

    private final int coordX;

    private final int coordY;

    Coordinate(int coordX, int coordY) {
      this.coordX = coordX;
      this.coordY = coordY;
    }

    @Override
    public boolean equals(Object obj) {
      Coordinate other = (Coordinate)obj;
      return coordX == other.coordX && coordY == other.coordY;
    }

  }

  class Lake {

    int surface;

  }

  enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;
  }

  enum CellType {
    EARTH,
    WATER;
  }

}
